package com.jdriven.reactive.casus;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Snapshot;
import com.codahale.metrics.Timer;
import com.jdriven.reactive.types.Kandidaat;
import com.jdriven.reactive.types.KandidaatInformatie;

@RunWith(Parameterized.class)
public class KandidaatInformatieSysteemTest {

    private static final int TEST_ITERATIONS = 50;
    private static final int WARM_UP_ITERATIONS = 5;

    @Parameter(0)
    public KandidaatInformatieSysteem kandidaatInformatieSysteem;

    @Parameter(1)
    public String omschrijving;

    private Logger logger;

    private AtomicInteger kandidaatId;

    private MetricRegistry metrics;

    @Parameters(name = "{1}")
    public static List<Object[]> getParameters() {
        return Arrays.asList(new Object[][] {
                { new NaiefKandidaatInformatieSysteem(), "Naief" },
                { new EnterpriseyKandidaatInformatieSysteem(), "Enterprisey" },
                { new ReactiveKandidaatInformatieSysteem(), "Reactive" }
        });
    }

    @Before
    public void setUp() {
        logger = LoggerFactory.getLogger(KandidaatInformatieSysteemTest.class.getSimpleName() + " - " + omschrijving);
        kandidaatId = new AtomicInteger();
        metrics = new MetricRegistry();
    }

    @Test
    public void testKandidaatInformatieSysteem() {
        logger.info("------------------------------------------------");
        logger.info("Measuring {} KandidaatInformatieSysteem", omschrijving);
        logger.info("------------------------------------------------");
        warmUp();

        Timer callTimer = metrics.timer("calls");
        double completenessScore = 0;

        logger.info("Running {} iterations...", TEST_ITERATIONS);
        for (int i = 0; i < TEST_ITERATIONS; i++) {
            logger.debug("Running iteration #{}/{}", i + 1, TEST_ITERATIONS);
            Timer.Context context = callTimer.time();
            try {
                KandidaatInformatie kandidaatInfo = kandidaatInformatieSysteem.getInfo(createKandidaat());
                context.stop();
                completenessScore += calculateCompleteness(kandidaatInfo);
            } catch (Exception e) {
                // ignore
            }
        }

        report(callTimer.getSnapshot(), completenessScore);
    }

    private void report(Snapshot snapshot, double completenessScore) {
        int successCount = snapshot.size();
        logger.info("# Iterations: {}", TEST_ITERATIONS);
        logger.info("# Completed: {} ({}%)", successCount, 100.0 * successCount / TEST_ITERATIONS);
        logger.info("Average profile completeness of completed calls: {}%", successCount == 0 ? 0 : completenessScore / successCount);
        logger.info("Avg. duration of completed calls: {} ms", TimeUnit.NANOSECONDS.toMillis((long) snapshot.getMean()));
        logger.info("Min. duration of completed calls: {} ms", TimeUnit.NANOSECONDS.toMillis(snapshot.getMin()));
        logger.info("Max. duration of completed calls: {} ms", TimeUnit.NANOSECONDS.toMillis(snapshot.getMax()));
    }

    private void warmUp() {
        logger.info("Warming up with {} iterations...", WARM_UP_ITERATIONS);
        for (int i = 0; i < WARM_UP_ITERATIONS; i++) {
            logger.debug("Warmup iteration #{}/{}", i + 1, WARM_UP_ITERATIONS);
            try {
                kandidaatInformatieSysteem.getInfo(createKandidaat());
            } catch (Exception e) {
                // ignore
            }
        }
    }

    private double calculateCompleteness(KandidaatInformatie kandidaatInfo) {
        int nrOfElementsPresent = 0;
        if (kandidaatInfo != null) {
            if (kandidaatInfo.getPersonalData() != null && !kandidaatInfo.getPersonalData().getData().isEmpty()) {
                nrOfElementsPresent++;
            }
            if (kandidaatInfo.getLinkedInBlogs() != null && !kandidaatInfo.getLinkedInBlogs().getData().isEmpty()) {
                nrOfElementsPresent++;
            }
            if (kandidaatInfo.getTweets() != null && !kandidaatInfo.getTweets().getData().isEmpty()) {
                nrOfElementsPresent++;
            }
        }
        return 100.0 * nrOfElementsPresent / 3;
    }

    private Kandidaat createKandidaat() {
        Integer id = kandidaatId.getAndIncrement();
        return new Kandidaat(id, id + "-voornaam", id + "-achternaam");
    }
}
