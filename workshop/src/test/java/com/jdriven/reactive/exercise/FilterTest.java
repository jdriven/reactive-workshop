package com.jdriven.reactive.exercise;

import org.junit.Test;
import rx.Observable;
import rx.observers.TestSubscriber;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Filter the stream of items emitted by Observables
 * see: https://github.com/ReactiveX/RxJava/wiki/Filtering-Observables
 */
public class FilterTest {

    /**
     * From the List of Integers filter out the even numbers
     *
     * @param list List of Integer
     * @return Observable of Integer
     */
    public Observable<Integer> filter(final List<Integer> list) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * From the List of Strings take the first two
     *
     * @param list List of String
     * @return Observable of String
     */
    public Observable<String> take(final List<String> list) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * From the List of Integers return that list without the first [count] elements.
     *
     * @param list List of Integer
     * @param count Integer number of items to skip
     * @return Observable of Integer
     */
    public Observable<Integer> skip(final List<Integer> list, final Integer count) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * From the List of Integers list remove all duplicates.
     *
     * @param list List of Integer
     * @return Observable of Integer
     */
    public Observable<Integer> distinct(final List<Integer> list) {
        return Observable.error(new RuntimeException("Implement me!"));
    }


    //
    // -------------------------------------------------------------------
    //


    @Test
    public void filterTest() {
        TestSubscriber<Integer> ts = new TestSubscriber<>();
        filter(range(1, 10)).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Arrays.asList(1, 3, 5, 7, 9));
    }

    @Test
    public void takeTest() {
        TestSubscriber<String> ts = new TestSubscriber<>();
        take(Arrays.asList("One", "Two", "Three", "Four")).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Arrays.asList("One", "Two"));
    }

    @Test
    public void skipTest() {
        TestSubscriber<Integer> ts = new TestSubscriber<>();
        skip(range(1, 10), 5).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Arrays.asList(6, 7, 8, 9));
    }

    @Test
    public void distinctTest() {
        TestSubscriber<Integer> ts = new TestSubscriber<>();
        final List<Integer> check = IntStream
                .concat(IntStream.range(1, 10), IntStream.range(1, 10))
                .boxed()
                .collect(Collectors.toList());
        distinct(check).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(range(1, 10));
    }



    private static List<Integer> range(final Integer lo, final Integer hi) {
        return IntStream.range(lo, hi).boxed().collect(Collectors.toList());
    }
}
