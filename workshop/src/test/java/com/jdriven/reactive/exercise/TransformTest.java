package com.jdriven.reactive.exercise;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static rx.Observable.just;
import static rx.Observable.range;

import com.jdriven.reactive.types.Kandidaat;
import com.jdriven.reactive.types.Kandidaten;
import org.junit.Test;
import rx.Observable;
import rx.observers.TestSubscriber;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

/**
 * Transform the items emitted by an Observable.
 * see: https://github.com/ReactiveX/RxJava/wiki/Transforming-Observables
 */
public class TransformTest {

    /**
     * Use reduce to retrieve the maximum Integer in the provided [ints].
     *
     * @param ints Observable of Integer
     * @return Observable of Integer
     */
    public Observable<Integer> max(Observable<Integer> ints) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * Transform the incoming parameter [name] with a prefix of "Hello "
     *
     * @param name String your name
     * @return Observable of String "Hello " + name
     */
    public Observable<String> map(final String name) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * Filter the incoming Integers to return only the even numbers and append " (even)" to those even Integers.
     *
     * @param ints Observable of Integer
     * @return Observable of even Integer
     */
    public Observable<String> filterMap(Observable<Integer> ints) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * Concatenate the incoming lists of Kandidaten and map the resulting list of Kandidaat to their ids.
     *
     * @param kandidaten Observable of Kandidaten
     * @return Observable of Integer
     */
    public Observable<Integer> concatMap(Observable<Kandidaten> kandidaten) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * Basically the same as concatMap, but using preferred method flatMap. In observable streams it is almost always
     * flatMap that should be used: flatMap uses merge instead of concat and allows multiple concurrent streams.
     *
     * @param kandidaten Observable of Kandidaten
     * @return Observable of Integer
     */
    public Observable<Integer> flatMap(Observable<Kandidaten> kandidaten) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * Use zip to merge two Observables of String into one.
     * a -> "eerste keus", "tweede", "suffe", "kansloze"
     * b -> "kandidaat", "kandidaat", "kandidaat", "kandidaat"
     * return -> "eerste keus kandidaat", "tweede kandidaat", "suffe kandidaat", "kansloze kandidaat"
     *
     * @param a Observable of String
     * @param b Observable of String
     * @return Observable of String
     */
    public Observable<String> zip(Observable<String> a, Observable<String> b) {
        return Observable.error(new RuntimeException("Implement me!"));
    }


    //
    // -------------------------------------------------------------------
    //


    @Test
    public void maxTest() {
        TestSubscriber<Integer> ts = new TestSubscriber<>();
        max(just(3, 6, 8, 9, 4, 12, 4, 2)).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Collections.singletonList(12));
    }

    @Test
    public void mapTest() {
        TestSubscriber<String> ts = new TestSubscriber<>();
        map("Hello").subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        assertEquals(1, ts.getOnNextEvents().size());
        assertTrue(ts.getOnNextEvents().get(0).startsWith("Hello "));
    }

    @Test
    public void filterMapTest() {
        TestSubscriber<String> ts = new TestSubscriber<>();
        filterMap(range(1, 10)).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Arrays.asList("2 (even)", "4 (even)", "6 (even)", "8 (even)", "10 (even)"));
    }

    @Test
    public void concatMapTest() {
        TestSubscriber<Integer> ts = new TestSubscriber<>();
        concatMap(kandidaten).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Arrays.asList(70111470, 654356453, 65432445, 675465));
    }

    @Test
    public void flatMapTest() {
        Map<Integer, Integer> map = flatMap(kandidaten).toMap(i -> i).toBlocking().single();
        assertTrue(map.containsKey(70111470));
        assertTrue(map.containsKey(654356453));
        assertTrue(map.containsKey(65432445));
        assertTrue(map.containsKey(675465));
    }

    @Test
    public void zipTest() {
        TestSubscriber<String> ts = new TestSubscriber<>();
        zip(
            just("eerste keus", "tweede", "suffe", "kansloze"),
            just("kandidaat", "kandidaat", "kandidaat", "kandidaat")
        ).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Arrays.asList(
                "eerste keus kandidaat", "tweede kandidaat", "suffe kandidaat", "kansloze kandidaat"));
    }


    private final Observable<Kandidaten> kandidaten = just(
            new Kandidaten("Architect", Arrays.asList(
                    new Kandidaat(70111470, "Bill", "Gates"),
                    new Kandidaat(654356453, "Ben", "Christensen"))),
            new Kandidaten("Java Developer", Arrays.asList(
                    new Kandidaat(65432445, "James", "Gosling"),
                    new Kandidaat(675465, "Armin", "van", "Buren"))));
}
