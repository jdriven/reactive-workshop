package com.jdriven.reactive.exercise;

import java.math.BigInteger;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import rx.Observable;
import rx.observers.TestSubscriber;

/**
 * Special assignments, bit more difficult than the previous exercises...
 */
public class SpecialsTest {

    /**
     * Return the Fibonacci numbers up until the [count].
     * See: https://en.wikipedia.org/wiki/Fibonacci_number
     *
     * hint: use scan(...)
     *
     * @param count Integer number of Fibonacci numbers to return
     * @return Observable of BigInteger
     */
    public Observable<BigInteger> fibonacci(final Integer count) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * The Project Euler #1: return the sum of the list of integers,
     * starting from 1 until [count] (inclusive), excluding the integers that are not dividable by 3 or 5.
     * See: https://projecteuler.net/problem=1
     *
     * @param count Integer where to stop...
     * @return Observable of Integer
     */
    public Observable<Integer> eulerOne(final Integer count) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * Replace each number divisable by three with 'fizz' and each number divisable by five with 'buzz'.
     * See: http://en.wikipedia.org/wiki/Fizz_buzz
     *
     * @param count Integer the number to count to
     * @return Observable of String
     */
    public Observable<String> fizzBuzz(final Integer count) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * Create an observable that emits "tick!" every second (= 1000 millisecond events)
     *
     * hint: start with time().
     *
     * @return Observable of String
     */
    public Observable<String> tick() {
        return Observable.error(new RuntimeException("Implement me!"));
    }


    //
    // -------------------------------------------------------------------
    //

    /**
     * Returns an Observable that emits the current time every millisecond.
     */
    public static Observable<LocalTime> time() {
        Clock clock = Clock.fixed(Instant.parse("2000-01-01T12:00:00.00Z"), ZoneId.systemDefault());

        return Observable.interval(1, TimeUnit.MILLISECONDS)
                .map(i -> LocalTime.now(Clock.offset(clock, Duration.ofMillis(i))));
    }


    @Test
    public void fibonacciTest() {
        TestSubscriber<BigInteger> ts = new TestSubscriber<>();
        fibonacci(10).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(
                Arrays.asList(
                        BigInteger.ZERO,
                        BigInteger.ONE,
                        BigInteger.ONE,
                        BigInteger.valueOf(2),
                        BigInteger.valueOf(3),
                        BigInteger.valueOf(5),
                        BigInteger.valueOf(8),
                        BigInteger.valueOf(13),
                        BigInteger.valueOf(21),
                        BigInteger.valueOf(34)
                )
        );
    }

    @Test
    public void noThreesOrFivesTest() {
        TestSubscriber<Integer> ts = new TestSubscriber<>();
        eulerOne(100).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Collections.singletonList(2418));
    }

    @Test
    public void fizzBuzzTest() {
        TestSubscriber<String> ts = new TestSubscriber<>();
        fizzBuzz(21).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Arrays.asList("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11",
                "Fizz", "13", "14", "FizzBuzz", "16", "17", "Fizz", "19", "Buzz", "Fizz"));
    }

    @Test
    public void tickTest() {
        TestSubscriber<String> ts = new TestSubscriber<>();
        tick().subscribe(ts);
        ts.awaitTerminalEvent(3, TimeUnit.SECONDS);
        ts.assertValues("tick!", "tick!", "tick!");
    }
}
