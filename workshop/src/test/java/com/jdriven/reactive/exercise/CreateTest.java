package com.jdriven.reactive.exercise;

import org.junit.Test;
import rx.Observable;
import rx.observers.TestSubscriber;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * First set of excercises: creating Observables.
 * see: https://github.com/ReactiveX/RxJava/wiki/Creating-Observables
 *  NB: this page also lists all the other methods to create Observables not covered here.
 */
public class CreateTest {

    /**
     * This method returns just the two words "Hello" and "World!"
     *
     * @return Observable of String
     */
    public Observable<String> hello() {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * This methods returns the specified List of String as an Observable
     *
     * @param list List of String
     * @return Observable of String
     */
    public Observable<String> fromList(final List<String> list) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * This method returns an empty Observable
     *
     * @return Observable of String
     */
    public Observable<String> empty() {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * This method returns the specified word [n] times.
     *
     * @param word String word to repeat
     * @param n Integer times to repeat specified word
     * @return Observable of String
     */
    public Observable<String> repeat(final String word, final Integer n) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * Create a range of [n] Integers, starting from [start].
     *
     * @param start Integer start of range
     * @param n Integer number of Integers to generate
     * @return Observable of Integer
     */
    public Observable<Integer> range(Integer start, Integer n) {
        return Observable.error(new RuntimeException("Implement me!"));
    }


    //
    // -------------------------------------------------------------------
    //


    @Test
    public void helloTest() {
        TestSubscriber<String> ts = new TestSubscriber<>();
        hello().subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Arrays.asList("Hello", "World!"));
    }

    @Test
    public void fromListTest() {
        List<String> list = Arrays.asList("Welcome", "to", "RxJava");
        TestSubscriber<String> ts = new TestSubscriber<>();
        fromList(list).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Arrays.asList("Welcome", "to", "RxJava"));
    }

    @Test
    public void emptyTest() {
        TestSubscriber<String> ts = new TestSubscriber<>();
        empty().subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Collections.emptyList());
    }

    @Test
    public void repeatTest() {
        TestSubscriber<String> ts = new TestSubscriber<>();
        repeat("JDriven", 10).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(
                Arrays.asList("JDriven",
                              "JDriven",
                              "JDriven",
                              "JDriven",
                              "JDriven",
                              "JDriven",
                              "JDriven",
                              "JDriven",
                              "JDriven",
                              "JDriven")
        );
    }

    @Test
    public void execerciseRange() {
        TestSubscriber<Integer> ts = new TestSubscriber<>();
        range(1, 10).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
    }

}
