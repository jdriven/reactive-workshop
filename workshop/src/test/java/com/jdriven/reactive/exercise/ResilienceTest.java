package com.jdriven.reactive.exercise;

import static rx.Observable.error;

import org.junit.Test;
import rx.Observable;
import rx.observers.TestSubscriber;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Just a brief first sniff of the resilience facilities of RxJava.
 */
public class ResilienceTest {

    /**
     * Leave the incoming data as is, but handle errors by returning a 'default-value'.
     *
     * @param data Observable of String
     * @return Observable of String
     */
    public Observable<String> handleError(Observable<String> data) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

    /**
     * The incoming data stream can have failures, so return that stream with added retry handling.
     *
     * @param data Observable of String
     * @return Observable of String
     */
    public Observable<String> retry(Observable<String> data) {
        return Observable.error(new RuntimeException("Implement me!"));
    }


    //
    // -------------------------------------------------------------------
    //


    @Test
    public void handleErrorTest() {
        TestSubscriber<String> ts = new TestSubscriber<>();
        handleError(error(new RuntimeException("failure"))).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Collections.singletonList("default-value"));
    }

    @Test
    public void retryTest() {
        TestSubscriber<String> ts = new TestSubscriber<>();
        AtomicInteger c = new AtomicInteger();
        Observable<String> o = Observable.create(s -> {
            if (c.incrementAndGet() <= 1) {
                s.onError(new RuntimeException("fail"));
            } else {
                s.onNext("success!");
                s.onCompleted();
            }
        });
        retry(o).subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertNoErrors();
        ts.assertReceivedOnNext(Collections.singletonList("success!"));
    }
}
