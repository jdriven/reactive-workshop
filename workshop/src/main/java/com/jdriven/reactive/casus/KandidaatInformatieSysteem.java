package com.jdriven.reactive.casus;

import com.jdriven.reactive.service.LinkedInService;
import com.jdriven.reactive.service.OAuthService;
import com.jdriven.reactive.service.PersonalDataService;
import com.jdriven.reactive.service.TwitterService;
import com.jdriven.reactive.types.Kandidaat;
import com.jdriven.reactive.types.KandidaatInformatie;

public abstract class KandidaatInformatieSysteem {

    private final PersonalDataService personalDataService = new PersonalDataService();

    private final LinkedInService linkedInService = new LinkedInService();

    private final TwitterService twitterService = new TwitterService();

    private final OAuthService oAuthService = new OAuthService();

    public abstract KandidaatInformatie getInfo(Kandidaat kandidaat);

    protected String getKey(Kandidaat kandidaat) {
        return kandidaat.toString();
    }

    protected PersonalDataService getPersonalDataService() {
        return personalDataService;
    }

    protected LinkedInService getLinkedInService() {
        return linkedInService;
    }

    protected TwitterService getTwitterService() {
        return twitterService;
    }

    protected OAuthService getoAuthService() {
        return oAuthService;
    }
}
