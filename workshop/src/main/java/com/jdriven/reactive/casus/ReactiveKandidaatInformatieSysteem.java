package com.jdriven.reactive.casus;

import rx.Observable;

import com.jdriven.reactive.types.Kandidaat;
import com.jdriven.reactive.types.KandidaatInformatie;

/**
 * Reactive...
 */
public class ReactiveKandidaatInformatieSysteem extends KandidaatInformatieSysteem {

    @Override
    public KandidaatInformatie getInfo(Kandidaat kandidaat) {
        String key = getKey(kandidaat);
        return getKandidaatInformatie(key).toBlocking().first();
    }

    private Observable<KandidaatInformatie> getKandidaatInformatie(final String key) {
        return Observable.error(new RuntimeException("Implement me!"));
    }

}
