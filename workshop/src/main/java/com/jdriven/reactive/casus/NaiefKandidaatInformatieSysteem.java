package com.jdriven.reactive.casus;

import com.jdriven.reactive.types.Kandidaat;
import com.jdriven.reactive.types.KandidaatInformatie;

/**
 * Non-reactive, no error handling...
 */
public class NaiefKandidaatInformatieSysteem extends KandidaatInformatieSysteem {

    @Override
    public KandidaatInformatie getInfo(Kandidaat kandidaat) {
        String key = getKey(kandidaat);

        KandidaatInformatie info = new KandidaatInformatie();

        info.setPersonalData(getPersonalDataService().get(key));

        String oauthToken = getoAuthService().get(key);

        info.setLinkedInBlogs(getLinkedInService().get(key, oauthToken));

        info.setTweets(getTwitterService().get(key));

        return info;
    }

}
