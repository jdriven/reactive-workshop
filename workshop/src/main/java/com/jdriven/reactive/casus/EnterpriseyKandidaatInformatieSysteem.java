package com.jdriven.reactive.casus;

import com.jdriven.reactive.types.Kandidaat;
import com.jdriven.reactive.types.KandidaatInformatie;

/**
 * Non-reactive, with error handling...
 */
public class EnterpriseyKandidaatInformatieSysteem extends KandidaatInformatieSysteem {

    @Override
    public KandidaatInformatie getInfo(Kandidaat kandidaat) {
        String key = getKey(kandidaat);
        KandidaatInformatie info = new KandidaatInformatie();

        fillPersonalData(info, key);
        fillLinkedInBlogs(info, key);
        fillTweets(info, key);

        return info;
    }

    private void fillPersonalData(KandidaatInformatie info, String key) {
        try {
            info.setPersonalData(getPersonalDataService().get(key));
        } catch (Exception e) {
            // Complain about not being able to fetch personal data in the application log file
        }
    }

    private void fillLinkedInBlogs(KandidaatInformatie info, String key) {
        String oauthToken = getOauthToken(key);
        if (oauthToken != null) {
            try {
                info.setLinkedInBlogs(getLinkedInService().get(key, oauthToken));
            } catch (Exception e) {
                // Complain about not being able to fetch LinkedIn data in the application log file
            }
        }
    }

    private String getOauthToken(String key) {
        String oauthToken = null;

        try {
            oauthToken = getoAuthService().get(key);
        } catch (Exception e) {
            // Complain about not being able to fetch Oauth token in the application log file
        }

        return oauthToken;
    }

    private void fillTweets(KandidaatInformatie info, String key) {
        try {
            info.setTweets(getTwitterService().get(key));
        } catch (Exception e) {
            // Complain about not being able to fetch tweets in the application log file
        }
    }

}
