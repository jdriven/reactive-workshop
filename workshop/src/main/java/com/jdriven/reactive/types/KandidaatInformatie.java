package com.jdriven.reactive.types;

import com.jdriven.reactive.domain.LinkedInBlogs;
import com.jdriven.reactive.domain.PersonalData;
import com.jdriven.reactive.domain.Tweets;

public class KandidaatInformatie {

    private PersonalData personalData;

    private Tweets tweets;

    private LinkedInBlogs linkedInBlogs;

    public KandidaatInformatie() {
        // default
    }

    public KandidaatInformatie(PersonalData personalData, Tweets tweets, LinkedInBlogs linkedInBlogs) {
        this.personalData = personalData;
        this.tweets = tweets;
        this.linkedInBlogs = linkedInBlogs;
    }

    public PersonalData getPersonalData() {
        return personalData;
    }

    public void setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
    }

    public Tweets getTweets() {
        return tweets;
    }

    public void setTweets(Tweets tweets) {
        this.tweets = tweets;
    }

    public LinkedInBlogs getLinkedInBlogs() {
        return linkedInBlogs;
    }

    public void setLinkedInBlogs(LinkedInBlogs linkedInBlogs) {
        this.linkedInBlogs = linkedInBlogs;
    }
}
