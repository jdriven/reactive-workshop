package com.jdriven.reactive.types;

/**
 * @author juvor
 * @since 15/05/15.
 */
public class Kandidaat {

  private Integer id;
  private String  voornaam;
  private String  tussenvoegsels;
  private String  achternaam;

  public Kandidaat(Integer id, String voornaam, String achternaam) {
    this.id = id;
    this.voornaam = voornaam;
    this.achternaam = achternaam;
  }

  public Kandidaat(Integer id, String voornaam, String tussenvoegsels, String achternaam) {
    this(id, voornaam, achternaam);
    this.tussenvoegsels = tussenvoegsels;
  }

  public Integer getId() { return id; }
  public String getVoornaam() { return voornaam; }
  public String getTussenvoegsels() { return tussenvoegsels; }
  public String getAchternaam() { return achternaam; }

  @Override
  public String toString() {
    return "" + id + ": " + voornaam + (tussenvoegsels != null ? " " + tussenvoegsels : "") + " " + achternaam;
  }
}
