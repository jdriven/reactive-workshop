package com.jdriven.reactive.types;

import rx.Observable;

import java.util.LinkedList;
import java.util.List;

/**
 * @author juvor
 * @since 15/05/15.
 */
public class Kandidaten {

  private String categorie;
  private List<Kandidaat> kandidaten = new LinkedList<>();

  public Kandidaten(String categorie, List<Kandidaat> kandidaten) {
    this.categorie = categorie;
    this.kandidaten.addAll(kandidaten);
  }

  public String getCategorie() { return categorie; }
  public Observable<Kandidaat> getKandidaten() { return Observable.from(kandidaten); }
}
