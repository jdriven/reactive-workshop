# Reactive Workshop #

The Reactive Workshop consists of three parts:
buzz word bingo,
reactive extensions
and
rewriting an imperative program in a more reactive way.

Focus is on making clear what a reactive application is
and illustrating the improvements reactive programming offers over a more classic style of programming.

## Prerequisites ##

* Laptop
* Java Development Kit (JDK) 8 installed on your laptop
* Your favorite IDE and/or Text Editor
* Gradle 2.3 / Maven 3.x

## Case ##

We will improve on an existing Candidate Information System, which is in use @ JDriven.
This application has a persistent store of Candidate profiles, and enriches our own
profile with information from Twitter, LinkedIn, and possibly more. The actual CV/resume is
stored on a disk somewhere in the vast JDriven network.

The external data sources can be flaky at times and some are just plain unreliable.
The current program is not very resilient or fault tolerant.

All of the mentioned 'external' services are implemented as local mocks that allow for
tweaking of the flakiness. There will be no requirement to access outside services.
